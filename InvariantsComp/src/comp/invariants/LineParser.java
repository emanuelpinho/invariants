package comp.invariants;

import japa.parser.ast.expr.BinaryExpr;
import japa.parser.ast.expr.Expression;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class LineParser {
	
	static Pattern invDeclaration = Pattern.compile(" *@ *([a-zA-Z]+) *([a-zA-Z][a-zA-Z0-9]*) *([\\[\\]])([0-9]+(.[0-9]*)?),([0-9]+(.[0-9]*)?)([\\[\\]])");
	static Pattern plusplus = Pattern.compile("(([a-zA-Z][a-zA-Z0-9]*)\\+\\+)");
	static Pattern minusminus = Pattern.compile("(([a-zA-Z][a-zA-Z0-9]*)--)");
	static Pattern attr = Pattern.compile(".*([a-zA-Z][a-zA-Z0-9]*)( )*=((.*))");
	
	static Pattern singleVar = Pattern.compile("([a-zA-Z][a-zA-Z0-9]*)");
	static Pattern method = Pattern.compile("([a-zA-Z])*\\.([a-zA-Z][a-zA-Z0-9]*)\\((.*)\\)");
	
	static Pattern binaryExpr = Pattern.compile("([a-zA-Z][a-zA-Z0-9]*)*");
	
	static public String parseLine(String line) {
		line = parsePlusPlus(line);
		line = parseMinusMinus(line);
		
		Matcher attrM = attr.matcher(line);
		if(attrM.matches()) {
			String leftSide = attrM.group(1);
			String rightSide = attrM.group(3);
			//line = parseAttr(leftSide, rightSide);
		}
		
		return line;
	}
	/*
	static private String parseAttr(String leftSide, String rightSide) {
		//String result = leftSide + ".setValue(" + parseAritmetic(rightSide) + ")";
		return result;
	}*/
	
	static public String parseAritmetic(String s, Hash varTable) {
		HashMap<String, Boolean> replaced = new HashMap<String, Boolean>();
		Matcher singleVarM = singleVar.matcher(s);
		
		while (singleVarM.find()) {
			String match = singleVarM.group();
			if(! varTable.contains(match))
				continue;
			Matcher methodM = method.matcher(match);
			if(methodM.matches() == false) {
				if(!replaced.containsKey(match)) {
					System.out.println(match);
					//match = match.replace("/", "");
					match = match.replace(" ", "");
					//System.out.println("--" + match);
					
					s = s.replace(match, match + ".getValue()");
					//s = s.replaceAll("[^a-z^A-Z^.^\\.]"+match+"[^a-z^A-Z^.^\\.]?", match + ".getValue()");
					//System.out.println(s);
					replaced.put(match, true);
				}
			}
		}
		return s;
	}
	
	static public Expression parseAritmetic2(Expression e, Hash varTable) {
		System.out.println(e);
		if(e instanceof BinaryExpr) {
			//System.out.println("x");
		}
		return e;
		
	}
	
	@SuppressWarnings("unused")
	static public JSONObject parseInvComment(String s) {
		Matcher invComment = invDeclaration.matcher(s);
		JSONObject jsonObj = null;
		if(invComment.matches()) {
			jsonObj = new JSONObject();
			jsonObj.put("type", invComment.group(1));
			jsonObj.put("name", invComment.group(2));
			jsonObj.put("firstLimit", invComment.group(3));
			jsonObj.put("value1", invComment.group(4));
			jsonObj.put("value2", invComment.group(6));
			jsonObj.put("secondLimit", invComment.group(8));
			
			
			/*
			System.out.println("1: " + invComment.group(1));
			System.out.println("2: " + invComment.group(2));
			System.out.println("3: " + invComment.group(3));
			System.out.println("4: " + invComment.group(4));
			System.out.println("6: " + invComment.group(6));
			System.out.println("8: " + invComment.group(8));
		*/
		}
		return jsonObj;
	}
	
	static private String parsePlusPlus(String s) {
		HashMap<String, Boolean> replaced = new HashMap<String, Boolean>();
		Matcher plusplusM = plusplus.matcher(s);
		while(plusplusM.find()) {
			String match = plusplusM.group();
			String varName = match.replace("++","");
			if(!replaced.containsKey(match)) {
				s = s.replace(match, varName + ".setValue(" + varName + ".getValue() + 1)");
				replaced.put(match, true);
			}
		}
		return s;
	}
	static private String parseMinusMinus(String s) {
		HashMap<String, Boolean> replaced = new HashMap<String, Boolean>();
		Matcher minusminusM = minusminus.matcher(s);
		while(minusminusM.find()) {
			String match = minusminusM.group();
			String varName = match.replace("--","");
			if(!replaced.containsKey(match)) {
				s = s.replace(match, varName + ".setValue(" + varName + ".getValue() - 1)");
				replaced.put(match, true);
			}
		}
		return s;
	}
	
	static private String parseBinaryExpr(String s, Hash varTable) {
		Matcher binaryExprM = binaryExpr.matcher(s);
		if(binaryExprM.matches()) {
			System.out.println("MATCHES");
		}
		return s;
		
	}
}

package comp.invariants;

import java.util.HashMap;

public class Hash<T> {
	
	private HashMap<String, Invariant<T>> invs;
	
	public Hash() {
		invs = new HashMap<String, Invariant<T>>();
	}
	
	public void addInv(String name, Invariant<T> inv){
		if(invs.containsKey(name))
			throw new InvariantReDeclaration();
		invs.put(name, inv);
	}
	
	public Invariant<T> getInvariant(String name){
		return invs.get(name);
	}
	
	public void print(){
		System.out.println("Tamanho da hash: " + invs.size());
		System.out.println("Representacao da hash: " + invs.toString());
	}

	public Boolean contains(String varName) {
		if(invs.containsKey(varName))
			return true;
		return false;
	}
}

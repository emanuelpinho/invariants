package comp.invariants;

public class OutOfRangeException extends RuntimeException {
	private static final long serialVersionUID = -9047919905994485221L;
	public OutOfRangeException() { super(); }
	public OutOfRangeException(String s) { super(s); }
	public OutOfRangeException(String s, Throwable throwable) { super(s, throwable); }
	public OutOfRangeException(Throwable throwable) { super(throwable); }
}
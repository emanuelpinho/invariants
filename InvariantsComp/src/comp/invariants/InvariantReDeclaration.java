package comp.invariants;

public class InvariantReDeclaration  extends RuntimeException{

	private static final long serialVersionUID = 7922629910892827402L;
	
	public InvariantReDeclaration() { super(); }
	public InvariantReDeclaration(String s) { super(s); }
	public InvariantReDeclaration(String s, Throwable throwable) { super(s, throwable); }
	public InvariantReDeclaration(Throwable throwable) { super(throwable); }

}

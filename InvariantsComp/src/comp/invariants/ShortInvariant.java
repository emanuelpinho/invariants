package comp.invariants;

public class ShortInvariant implements Invariant<Short> {

	Short first = null, last = null, value = null;
	Boolean firstInner, secondInner;
	
	public ShortInvariant(Short first, Short last, Boolean firstInner,  Boolean secondInner) {
		this.firstInner = firstInner;
		this.secondInner = secondInner;
		this.first = first;
		this.last = last;
	}
	
	@Override
	public void setValue(Short v) {
		if (firstInner) {
			if (v < first)
				throw new OutOfRangeException();
		}
		else {
			if (v <= first)
				throw new OutOfRangeException();
		}
		
		if (secondInner) {
			if (v > first)
				throw new OutOfRangeException();
		}
		else {
			if (v >= first)
				throw new OutOfRangeException();
		}	
		
		value = v;
	}

	@Override
	public Short getFirstValue() {
		return first;
	}

	@Override
	public Short getSecondValue() {
		return last;
	}

	@Override
	public Short getValue() {
		return value;
	}

}

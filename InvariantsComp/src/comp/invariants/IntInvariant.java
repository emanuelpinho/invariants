package comp.invariants;

public class IntInvariant  implements Invariant<Integer> {

	Integer first = null, last = null, value = null;
	Boolean firstInner, secondInner;
	
	public IntInvariant(Integer first, Integer last, Boolean firstInner,  Boolean secondInner) {
		this.firstInner = firstInner;
		this.secondInner = secondInner;
		this.first = first;
		this.last = last;
	}
	
	@Override
	public void setValue(Integer v) throws OutOfRangeException {
		if (firstInner) {
			if (v < first)
				throw new OutOfRangeException();
		}
		else {
			if (v <= first)
				throw new OutOfRangeException();
		}
		
		if (secondInner) {
			if (v > first)
				throw new OutOfRangeException();
		}
		else {
			if (v >= first)
				throw new OutOfRangeException();
		}	
		
		value = v;
	}

	@Override
	public Integer getFirstValue() {
		return first;
	}

	@Override
	public Integer getSecondValue() {
		return last;
	}

	@Override
	public Integer getValue() {
		return value;
	}

}

package comp.invariants;

public class DoubleInvariant  implements Invariant<Double> {

	Double first = null, last = null, value = null;
	Boolean firstInner, secondInner;
	
	public DoubleInvariant(Double first, Double last, Boolean firstInner,  Boolean secondInner) {
		this.firstInner = firstInner;
		this.secondInner = secondInner;
		this.first = first;
		this.last = last;
	}
	
	@Override
	public void setValue(Double v) {
		if (firstInner) {
			if (v < first)
				throw new OutOfRangeException();
		}
		else {
			if (v <= first)
				throw new OutOfRangeException();
		}
		
		if (secondInner) {
			if (v > first)
				throw new OutOfRangeException();
		}
		else {
			if (v >= first)
				throw new OutOfRangeException();
		}	
		
		value = v;
	}

	@Override
	public Double getFirstValue() {
		return first;
	}

	@Override
	public Double getSecondValue() {
		return last;
	}

	@Override
	public Double getValue() {
		return value;
	}

}

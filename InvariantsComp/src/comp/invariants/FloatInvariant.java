package comp.invariants;

public class FloatInvariant implements Invariant<Float> {

	Float first = null, last = null, value = null;
	Boolean firstInner, secondInner;
	
	public FloatInvariant(float first, float last, Boolean firstInner,  Boolean secondInner) {
		this.firstInner = firstInner;
		this.secondInner = secondInner;
		this.first = first;
		this.last = last;
	}
	
	@Override
	public void setValue(Float v) {
		if (firstInner) {
			if (v < first)
				throw new OutOfRangeException();
		}
		else {
			if (v <= first)
				throw new OutOfRangeException();
		}
		
		if (secondInner) {
			if (v > first)
				throw new OutOfRangeException();
		}
		else {
			if (v >= first)
				throw new OutOfRangeException();
		}
		value = v;
	}

	@Override
	public Float getFirstValue() {
		return first;
	}

	@Override
	public Float getSecondValue() {
		return last;
	}

	@Override
	public Float getValue() {
		return value;
	}

}

package comp.invariants;

public interface Invariant<T> {
	
	public void setValue(T v) throws OutOfRangeException;
	public T getFirstValue();
	public T getSecondValue();
	public T getValue();
}

package comp.app;

import comp.invariants.IntInvariant;
import comp.invariants.Invariant;

public class TestClass {

    //  @    int value ]1,2]
    IntInvariant value = new IntInvariant(1, 2, true, true);

    int value2 = 2;

    String phrase;

    int value3 = 3;

    public int nullMethod(int i) {
        // @ int x [1,5]
        IntInvariant x = new IntInvariant(1, 5, true, true);
        IntInvariant iv = new IntInvariant(1, 2, true, true), v = new IntInvariant(1, 2, true, true);
        // @ int value3 [1,4]
        IntInvariant value3 = new IntInvariant(1, 4, true, true);
        // comentario random sem significado
        int value5 = 5;
        value3.setValue(value5 * 10 + (5 + value3.getValue()) + iv.getValue());
        value5 = 10;
        x.setValue(2);
        for (x.setValue(1); x.getValue() < value3.getValue(); x.setValue(x.getValue() - 1), value5++) {
            value3++;
            // @ int valueFor [1,2]
            IntInvariant valueFor = new IntInvariant(1, 2, true, true);
        }
        if (value3.getValue() == 3) {
            value3.setValue(10 * value3.getValue());
        } else {
            value3.setValue(10 * value3.getValue());
        }
        while (value3.getValue() != 0) {
            value3.setValue(10 * value3.getValue());
        }
        return value3.getValue();
    }
}

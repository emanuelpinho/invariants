package comp.app;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.comments.Comment;
import japa.parser.ast.comments.LineComment;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.IntegerLiteralExpr;
import japa.parser.ast.expr.ObjectCreationExpr;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.ForStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.type.ClassOrInterfaceType;
import japa.parser.ast.type.PrimitiveType;
import japa.parser.ast.type.PrimitiveType.Primitive;
import japa.parser.ast.type.ReferenceType;
import japa.parser.ast.type.Type;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("deprecation")
public class AppOld {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws IOException, ParseException {
		
	
		String file = readFile("./src/comp/app/TestClass.java");
		CompilationUnit cu = JavaParser.parse(new StringBufferInputStream(file));
		
		TypeDeclaration td = cu.getTypes().get(0);
		
		List<BodyDeclaration> members = td.getMembers();
		//System.out.println(cu.getTypes().get(0).getMembers());
		
		for(int i = 0; i < members.size(); i++) {
			parseBodyDeclaration(members.get(i));
						
		}
		System.out.println("FINAL RESULT:\n" + cu.toString());
	}
	
	static void parseBodyDeclaration(BodyDeclaration bd) {
		
		if(bd instanceof FieldDeclaration) {
			FieldDeclaration fbd = (FieldDeclaration) bd;
			parseFieldDeclaration(fbd);
		} else if(bd instanceof MethodDeclaration) {
			MethodDeclaration mbd = (MethodDeclaration) bd;
			parseMethodDeclaration(mbd);
		}
		
	
	}
	
	private static void parseFieldDeclaration(FieldDeclaration fbd) {
		
		Type type = fbd.getType();
		
		int fbdBeginLine = fbd.getBeginLine();
		int fbdBeginCollumn = fbd.getBeginColumn();
		
		parseVariableDeclarations(fbd.getVariables());
		
		//fbd.setVariables(listVariableDeclarator);
	}
	
	private static void parseMethodDeclaration(MethodDeclaration mbd) {
		List<Statement> stmts = mbd.getBody().getStmts();
		for(int i = 0; i < stmts.size(); i++) {
			Statement stmt = stmts.get(i);
			if(stmt instanceof ExpressionStmt) {
				ExpressionStmt expStmt = (ExpressionStmt) stmt;
				Expression exp = expStmt.getExpression();
				if(exp instanceof VariableDeclarationExpr) {
					
					VariableDeclarationExpr vdExp = (VariableDeclarationExpr) exp;
					
					
					PrimitiveType ptype = new  PrimitiveType(Primitive.Float);
					
					vdExp.setType(ptype);
					
					//Comment cm = new LineComment("lalal");
					//vdExp.setComment(cm);
					if(vdExp.getComment() != null)
						System.out.println("COMMENT:" + vdExp.getComment());
					
					//TROCAR ESTES
					/*System.out.println("VARIABLE DECLARATION EXPRESSION");
					System.out.println(vdExp.getType());
					System.out.println(vdExp.getVars());
					System.out.println(stmt);
					*/
					
					parseVariableDeclarations(vdExp.getVars());
					
					//Type invariantInteger = new ClassOrInterfaceType(vdExp.getBeginLine(), vdExp.getBeginColumn(), null, "InvariantInteger", null);
					
					//VariableDeclarationExpr newVdExp = new VariableDeclarationExpr(vdExp.getBeginLine(), vdExp.getBeginColumn(),
						//	vdExp.getModifiers(), 0, 0, vdExp.getAnnotations(), vdExp.getType(), lVd);
					//ExpressionStmt newExpStmt = new ExpressionStmt(vdExp.getBeginLine(), vdExp.getBeginColumn(), 0, 0, newVdExp);
					
					
					//stmts.remove(i);
					//stmts.add(i, newExpStmt);
				}
			}
			//FieldDeclaration fd = (FieldDeclaration) stmt.getData();
			
		}
		
		//return null;
	}
	
	private static void parseVariableDeclarations(List<VariableDeclarator> lVd) {
		List<VariableDeclarator> result = new ArrayList<VariableDeclarator>();
		for(VariableDeclarator vd : lVd) {
			
			VariableDeclaratorId id =  vd.getId();
			
			
			
			//ClassOrInterfaceType invariantInteger = new ClassOrInterfaceType(0, 0, null, "InvariantInteger", null);
			ArrayList<Type> arrType = new ArrayList<Type>();
			//arrType.add(invariantInteger);
			//ReferenceType invariantReferenceType = new ReferenceType(vdBeginLine, vdBeginCollumn, invariantInteger, 0);
			
			
			PrimitiveType ptype = new  PrimitiveType(Primitive.Float);
			//init = new VariableDeclarationExpr(vd.getBeginLine(), vd.getBeginColumn(),
				//	1, null, ptype, lVd);
			
			vd.setId(new VariableDeclaratorId("TEST"));
			
			
			System.out.println(vd.getId() + " = " + vd.getInit());
			
			ObjectCreationExpr newInvariant = new ObjectCreationExpr();
			
			ClassOrInterfaceType intInvariantType = new ClassOrInterfaceType("InvariantInteger");
			newInvariant.setType(intInvariantType);
			
			vd.setInit(newInvariant);
			/*Expression init = vd.getInit();
			if(init != null) {
				System.out.println(init.getClass().getCanonicalName());
				if(init instanceof IntegerLiteralExpr) {
					IntegerLiteralExpr novo = (IntegerLiteralExpr) init;
					novo.setValue("101010");
					
				}
			}*/
			
			
			//vd.setInit(vdexp);
		}
		ClassOrInterfaceType invariantInteger = new ClassOrInterfaceType("IntInvariant");
		ReferenceType rInvariantInteger = new ReferenceType(invariantInteger);
		VariableDeclarationExpr vdexp = new VariableDeclarationExpr(rInvariantInteger,lVd);
		//System.out.println(vdexp);
	}
	
	private static String readFile(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        try {
            StringBuilder ret = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                ret.append(line);
                ret.append("\n");
            }
            return ret.toString();
        } finally {
            reader.close();
        }
    }
}

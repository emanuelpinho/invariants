package comp.app;

import comp.invariants.IntInvariant;
import comp.invariants.Invariant;

public class TestClass {
	
	
	//  @    int value ]1,2]
	int value = 1;
			
	int value2 = 2;
	
	String phrase;
	
	int value3 = 3;
	
	public int nullMethod(int i) {
		
		
		// @ int x [1,5]
		int x;
		
		IntInvariant iv = new IntInvariant(1,2,true,true), v = new IntInvariant(1,2,true,true);
		
		// @ int value3 [1,4]
		int value3 = 3;
		
		// comentario random sem significado
		int value5 = 5;
		
		value3 = value5*10 + (5 + value3) + iv.getValue();
		
		value5 = 10;
		
		x = 2;
		
		for(x = 1; x < value3; x--, value5++) {
			value3++;
			
			// @ int valueFor [1,2]
			int valueFor = 0;
		}
		if(value3 == 3) {
			
			value3 = 10 * value3;
		}
		else{
			value3 = 10 * value3;
		}
		
		while(value3 != 0){
			value3 = 10 * value3;
		}
		return value3;
	}
	
	
}

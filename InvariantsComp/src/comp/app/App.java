package comp.app;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.Node;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.body.VariableDeclaratorId;
import japa.parser.ast.comments.Comment;
import japa.parser.ast.expr.AssignExpr;
import japa.parser.ast.expr.BinaryExpr;
import japa.parser.ast.expr.BooleanLiteralExpr;
import japa.parser.ast.expr.EnclosedExpr;
import japa.parser.ast.expr.Expression;
import japa.parser.ast.expr.IntegerLiteralExpr;
import japa.parser.ast.expr.MethodCallExpr;
import japa.parser.ast.expr.NameExpr;
import japa.parser.ast.expr.ObjectCreationExpr;
import japa.parser.ast.expr.StringLiteralExpr;
import japa.parser.ast.expr.UnaryExpr;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.ForStmt;
import japa.parser.ast.stmt.IfStmt;
import japa.parser.ast.stmt.ReturnStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.stmt.WhileStmt;
import japa.parser.ast.type.ClassOrInterfaceType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import comp.invariants.DoubleInvariant;
import comp.invariants.FloatInvariant;
import comp.invariants.Hash;
import comp.invariants.IntInvariant;
import comp.invariants.Invariant;
import comp.invariants.LineParser;
import comp.invariants.ShortInvariant;

@SuppressWarnings("deprecation")
public class App {
	
	private static Hash propertyTable= null;
	/**
	 * @param args
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws IOException, ParseException {
		
		propertyTable = new Hash();
		String file = readFile("./src/comp/app/TestClass.java");
		CompilationUnit cu = JavaParser.parse(new StringBufferInputStream(file));
		
		TypeDeclaration td = cu.getTypes().get(0);
		
		List<BodyDeclaration> members = td.getMembers();
		
		for(int i = 0; i < members.size(); i++) {
			parseBodyDeclaration(members.get(i));
			//cada membro(atributos e funcoes) da classe principal
		}
		System.out.println("FINAL RESULT:\n" + cu.toString());
		App.storeFile("TestClass.java", cu.toString());
	}
	
	private static Boolean parseComments(Comment cmt, Hash varTable){
		if(cmt == null)
			return false;
		//System.out.println("Comentarios dos atributos:" + cmt.getContent()); //Nao precisa de ser javadoc 
		JSONObject jsonObj = LineParser.parseInvComment(cmt.getContent());
		if(jsonObj != null){
			String type = jsonObj.get("type").toString();
			String name = jsonObj.get("name").toString();
			
			Boolean firstLimit, secondLimit;
			
			if (jsonObj.get("firstLimit").toString().compareTo("]") == 0) {
				firstLimit = false;
			}
			else {
				firstLimit = true;
			}
			
			if (jsonObj.get("secondLimit").toString().compareTo("]") == 0) {
				secondLimit = true;
			}
			else {
				secondLimit = false;
			}
			
			if(varTable.contains(name)) {
				System.out.println("RE-DECLARACAO DE INVARIANTE; DEVE LANCAR EXCEPCAO");
				return false;
			}
			if(type.compareTo("int") == 0){
				int value1 = Integer.parseInt(jsonObj.get("value1").toString());
				int value2 = Integer.parseInt(jsonObj.get("value2").toString());
				varTable.addInv(name, new IntInvariant(value1, value2, firstLimit, secondLimit));
				
				return true;
			} else if(type.compareTo("float") == 0){
				float value1 = Float.parseFloat(jsonObj.get("value1").toString());
				float value2 = Float.parseFloat(jsonObj.get("value2").toString());
				varTable.addInv(name, new FloatInvariant(value1, value2, firstLimit, secondLimit));
				
				return true;
			} else if(type.compareTo("double") == 0){
				double value1 = Double.parseDouble(jsonObj.get("value1").toString());
				double value2 = Double.parseDouble(jsonObj.get("value2").toString());
				varTable.addInv(name, new DoubleInvariant(value1, value2, firstLimit, secondLimit));
				
				return true;
			} else if(type.compareTo("short") == 0){
				short value1 = Short.parseShort(jsonObj.get("value1").toString());
				short value2 = Short.parseShort(jsonObj.get("value2").toString());
				varTable.addInv(name, new ShortInvariant(value1, value2, firstLimit, secondLimit));
				
				return true;
			}
			
		}
		return false;
	}
	
	static void parseBodyDeclaration(BodyDeclaration bd) {
		if(bd instanceof FieldDeclaration) {
			//Atributos da classe principal
			FieldDeclaration fbd = (FieldDeclaration) bd; 
			if(fbd.getComment() != null){
				parseComments(fbd.getComment(), propertyTable);
			}
			parseFieldDeclaration(fbd);
		} else if(bd instanceof MethodDeclaration) {
			//Metodos da classe principal
			MethodDeclaration mbd = (MethodDeclaration) bd;
			//System.out.println("Comentarios das funcoes:" + mbd.getComment()); //Nao precisa de ser javadoc 
			parseMethodDeclaration(mbd);
		}
		
	
	}
	
	private static void parseFieldDeclaration(FieldDeclaration fbd) {
		
		if (propertyTable.getInvariant(fbd.getVariables().get(0).getId().toString()) != null) {		
			ClassOrInterfaceType invariantType = null;
			
			// AQUI TEMOS DE VERIFICAR SE ESTA NA HASH OU NAO, ESTA A ALTERAR TODOS
			if (fbd.getType().toString().compareTo("int") == 0) {
				invariantType = new ClassOrInterfaceType("IntInvariant");
			}
			else if (fbd.getType().toString().compareTo("float") == 0) {
				invariantType = new ClassOrInterfaceType("FloatInvariant");
			}
			else if (fbd.getType().toString().compareTo("double") == 0) {
				invariantType = new ClassOrInterfaceType("DoubleInvariant");
			}
			else if (fbd.getType().toString().compareTo("short") == 0) {
				invariantType = new ClassOrInterfaceType("ShortInvariant");
			}
			
			if (invariantType != null)
				fbd.setType(invariantType);
			
			parseVariableDeclarations(fbd.getVariables(),propertyTable);
		}
		
		
	}
	
	private static void parseVariableDeclarationExpr(Expression exp, Hash varTable){
		VariableDeclarationExpr vdExp = (VariableDeclarationExpr) exp;
		
		String varName = vdExp.getVars().get(0).getId().toString(); 
		
		if (varTable.getInvariant(varName) != null) {
		
			ClassOrInterfaceType invariantType = null;
			
			if (vdExp.getType().toString().compareTo("int") == 0) {
				invariantType = new ClassOrInterfaceType("IntInvariant");
			}
			else if (vdExp.getType().toString().compareTo("float") == 0) {
				invariantType = new ClassOrInterfaceType("FloatInvariant");
			}
			else if (vdExp.getType().toString().compareTo("double") == 0) {
				invariantType = new ClassOrInterfaceType("DoubleInvariant");
			}
			else if (vdExp.getType().toString().compareTo("short") == 0) {
				invariantType = new ClassOrInterfaceType("ShortInvariant");
			}
			if (invariantType != null)
				vdExp.setType(invariantType);		
			
			parseVariableDeclarations(vdExp.getVars(), varTable);
		
		}
	}
	
	private static void parseBinaryTree(Expression e, Hash varTable) {
		
		if(e instanceof NameExpr) {
			
			String name = ((NameExpr) e).getName();
			if(varTable.contains(name)) {
				
				((NameExpr) e).setName(name + ".getValue()");
			}
			
		}
		else if(e instanceof BinaryExpr) {
			Expression left = ((BinaryExpr) e).getLeft();
			Expression right = ((BinaryExpr) e).getRight();
			parseBinaryTree(left, varTable);
			parseBinaryTree(right, varTable);			
		} else if(e instanceof EnclosedExpr) {
			EnclosedExpr eexpr = (EnclosedExpr) e;
			List<Node> nodes = eexpr.getChildrenNodes();
			for(Node n : nodes) {
				if(n instanceof Expression) {
					parseBinaryTree((Expression) n, varTable);
				}
			}
			
		}
	}
	
	
	private static Expression parseUnaryExpr(UnaryExpr uExpr, Hash varTable) {
		System.out.println("UNARY");
		String varName = uExpr.getExpr().toString();
		String operator = uExpr.getOperator().toString();
		System.out.println(operator);
		if(operator.compareTo("posIncrement") == 0 || operator.compareTo("preIncrement") == 0) {
			operator = "+";
		} else if(operator.compareTo("posDecrement") == 0 || operator.compareTo("preDecrement") == 0) {
			operator = "-";
		}
		if(!varTable.contains(varName)) {
			return uExpr;
		}
		MethodCallExpr mcExpr = new MethodCallExpr();
		
		NameExpr varNameExpr = new NameExpr();
		varNameExpr.setName(varName);
		
		mcExpr.setScope(varNameExpr);
		
		NameExpr methodName = new NameExpr();
		
		List<Expression> expressionList = new ArrayList<Expression>();
		//expressionList.add(value);
		mcExpr.setArgs(expressionList);
		methodName.setName("setValue");
		mcExpr.setNameExpr(methodName);
		
		NameExpr newExpression = new NameExpr();
		newExpression.setName(varName + ".getValue() " + operator + " 1");
		List<Expression> args = new ArrayList<Expression>();
		args.add(newExpression);
		mcExpr.setArgs(args);
		
		
		return mcExpr;
		
	}
	
	private static Expression parseAssignExpr(AssignExpr aExpr, Hash varTable) {
		String varName = aExpr.getTarget().toString();
		
		if(!varTable.contains(varName)) {
			return aExpr;
		}
		
		Expression value = (Expression)aExpr.getValue();
		parseBinaryTree(value, varTable);
		//value.getL
		//String newValue = LineParser.parseAritmetic(value.toString(), varTable);
		
		
		
		MethodCallExpr mcExpr = new MethodCallExpr();
		
		NameExpr varNameExpr = new NameExpr();
		varNameExpr.setName(varName);
		
		mcExpr.setScope(varNameExpr);
		
		NameExpr methodName = new NameExpr();
		
		List<Expression> expressionList = new ArrayList<Expression>();
		expressionList.add(value);
		mcExpr.setArgs(expressionList);
		methodName.setName("setValue");
		mcExpr.setNameExpr(methodName);
		
		
		
		
		return mcExpr;
		
	}
	
	/**
	 * A Hash serve para registar todas as invariantes declaradas.
	 * @param stmt
	 * @param varTable
	 */
	private static void parseStmt(Statement stmt, Hash varTable){
		
		
		if(stmt instanceof ExpressionStmt) {
			
			
			
			ExpressionStmt expStmt = (ExpressionStmt) stmt;
			Expression exp = expStmt.getExpression();
			
			if(parseComments(stmt.getComment(), varTable)) {
				if(exp instanceof VariableDeclarationExpr) {
					parseVariableDeclarationExpr(exp, varTable);
				}
			} 
			else {
				if(exp instanceof AssignExpr) {
					AssignExpr aExpr = (AssignExpr) exp;
					
					exp = parseAssignExpr(aExpr, varTable);
					((ExpressionStmt) stmt).setExpression(exp);
					
					//System.out.println(exp.toString());
				}
				if(exp instanceof MethodCallExpr) {
					MethodCallExpr mcExpr = (MethodCallExpr) exp;
					
					// scope = iv
					// nameExpr = setValue
					// [1] = getArgs
					//System.out.println("METHOD CALL WLELELELLE: " + mcExpr.getNameExpr().getClass().getName());
					//System.out.println(mcExpr.getNameExpr().getClass().getName());
				}
			}
			
			
		}
		
		//Alguns destes em baixo devolvem stmt, a cena e criar uma funcao que receba stmts e altere conforme necessidade
		else if(stmt instanceof ForStmt){
			ForStmt forStmt = (ForStmt) stmt;
			List<Node> nodes = forStmt.getBody().getChildrenNodes();
			
			//System.out.println("Dentro do for: " + nodes);
			//System.out.println("Dentro do for: " + nodes.get(1).getClass().getName());
			for(int i = 0;i < nodes.size(); i++){
				Statement s = (Statement) nodes.get(i);
				parseStmt(s, varTable);
			}
			
			List<Expression> init = forStmt.getInit();
			for(int i = 0; i < init.size(); i++) {
				Expression initExpr = init.get(i);
				
				if(initExpr instanceof AssignExpr) {
					 Expression newAssign = parseAssignExpr((AssignExpr)initExpr,  varTable);
					 init.set(i, newAssign);
				}
			}
		
			
			Expression compare = forStmt.getCompare();
			
			parseBinaryTree(compare, varTable);
			
			
		
			List<Expression> update = forStmt.getUpdate();
			for(int c = 0; c < update.size(); c++) {
				Expression updateExpr = update.get(c);
				System.out.println(updateExpr.getClass().getName());
				if(updateExpr instanceof AssignExpr) {
					Expression newAssign = parseAssignExpr((AssignExpr)updateExpr,  varTable);
					update.set(c, newAssign);
				} else if(updateExpr instanceof UnaryExpr) {
					Expression newUnary = parseUnaryExpr((UnaryExpr) updateExpr, varTable);
					update.set(c, newUnary);
				}
			}
			//System.out.println("Body do for:" + forStmt.getBody());
		}
		else if(stmt instanceof IfStmt){
			IfStmt ifStmt = (IfStmt) stmt;
			Expression condition = ifStmt.getCondition();
			parseBinaryTree(condition, varTable);
			
			parseStmt(ifStmt.getThenStmt(),varTable);
			parseStmt(ifStmt.getElseStmt(),varTable);
			//System.out.println("If condition:" + ifStmt.getCondition());
			System.out.println("If body:" + ifStmt.getThenStmt().getClass().getName());
			//System.out.println("Else body:" + ifStmt.getElseStmt());
		}
		else if(stmt instanceof WhileStmt){
			WhileStmt whileStmt = (WhileStmt) stmt;
			parseStmt(whileStmt.getBody(),varTable);
			
			Expression condition = whileStmt.getCondition();
			parseBinaryTree(condition, varTable);
			//System.out.println("While condition:" + whileStmt.getCondition());
			//System.out.println("While condition:" + whileStmt.getBody());
		}
		else if(stmt instanceof ReturnStmt){
			ReturnStmt returnStmt = (ReturnStmt) stmt;
			parseBinaryTree(returnStmt.getExpr(), varTable); 
			//System.out.println("Return value: " + returnStmt.getExpr());
		}

		else if(stmt instanceof BlockStmt) {
			BlockStmt blockStmt = (BlockStmt) stmt;
			List<Node> nodes = blockStmt.getChildrenNodes();
			
			for(int i = 0;i < nodes.size(); i++){
				Statement s = (Statement) nodes.get(i);
				parseStmt(s, varTable);
			}			
		}
	}
	
	
	
	private static void parseMethodDeclaration(MethodDeclaration mbd) {
		//Statment e cada ";" da funcao
		Hash varTable = new Hash();
		List<Statement> stmts = mbd.getBody().getStmts();
		for(int i = 0; i < stmts.size(); i++) {
			Statement stmt = stmts.get(i);
			parseStmt(stmt, varTable);
		}
	}
	
	private static void parseVariableDeclarations(List<VariableDeclarator> lVd, Hash varTable) {
		for(VariableDeclarator vd : lVd) {
			//System.out.println("Variable declaration: " + vd.toString());
			
			
			//invariants.print();
			//System.out.println("Nome da variavel: " + vd.getId().toString());
			String name = vd.getId().toString();
			Invariant inv = varTable.getInvariant(vd.getId().toString());
			
			if (inv != null) {
						
				vd.setId(new VariableDeclaratorId(name)); //NOME
				
				//System.out.println(vd.getId() + " = " + vd.getInit());
				
				
				
				//Cria a segunda parte da declaracao: new InInvariant(firstValue, secondValue, name); 
				ObjectCreationExpr newInvariant = new ObjectCreationExpr();
				StringLiteralExpr nameExpr = new StringLiteralExpr(name);
				ArrayList<Expression> argsListExpr = new ArrayList<Expression>();
				
				if(inv instanceof IntInvariant){
					ClassOrInterfaceType intInvariantType = new ClassOrInterfaceType("IntInvariant");
					newInvariant.setType(intInvariantType);
					IntegerLiteralExpr firstValue = new IntegerLiteralExpr(inv.getFirstValue().toString());
					IntegerLiteralExpr secondValue = new IntegerLiteralExpr(inv.getSecondValue().toString());
					argsListExpr.add(firstValue);
					argsListExpr.add(secondValue);
					BooleanLiteralExpr b = new BooleanLiteralExpr(true);
					argsListExpr.add(b);
					argsListExpr.add(b);
				}
				
				//argsListExpr.add(nameExpr);
				
				newInvariant.setArgs(argsListExpr);
				vd.setInit(newInvariant);
			}
			
		}
	
	}
	
	private static String readFile(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
        try {
            StringBuilder ret = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                ret.append(line);
                ret.append("\n");
            }
            return ret.toString();
        } finally {
            reader.close();
        }
    }
	
	private static void storeFile(String filename, String content) {
		FileOutputStream fop = null;
		try {
			 
			File file = new File("./src/comp/app/GEN" + filename);
			fop = new FileOutputStream(file);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			// get the content in bytes
			byte[] contentInBytes = content.getBytes();
 
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
